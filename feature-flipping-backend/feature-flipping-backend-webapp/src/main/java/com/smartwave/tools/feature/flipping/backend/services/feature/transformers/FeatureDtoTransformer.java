package com.smartwave.tools.feature.flipping.backend.services.feature.transformers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.smartwave.tools.feature.flipping.backend.api.models.DtoTransformer;
import com.smartwave.tools.feature.flipping.backend.api.models.Feature;
import com.smartwave.tools.feature.flipping.backend.api.models.FeatureBuilder;
import com.smartwave.tools.feature.flipping.backend.api.models.rules.CanalFilter;
import com.smartwave.tools.feature.flipping.backend.api.models.rules.HeaderFilter;
import com.smartwave.tools.feature.flipping.backend.api.models.rules.RoleFilter;
import com.smartwave.tools.feature.flipping.backend.api.models.rules.Rule;
import com.smartwave.tools.feature.flipping.backend.api.models.rules.RuleBuilder;
import com.smartwave.tools.feature.flipping.backend.api.models.rules.SchedulerFilter;
import com.smartwave.tools.feature.flipping.backend.api.models.rules.UserFilter;
import com.smartwave.tools.feature.flipping.backend.services.feature.entities.CanalFilterEntity;
import com.smartwave.tools.feature.flipping.backend.services.feature.entities.FeatureEntity;
import com.smartwave.tools.feature.flipping.backend.services.feature.entities.HeaderFilterEntity;
import com.smartwave.tools.feature.flipping.backend.services.feature.entities.RoleFilterEntity;
import com.smartwave.tools.feature.flipping.backend.services.feature.entities.RuleEntity;
import com.smartwave.tools.feature.flipping.backend.services.feature.entities.SchedulerFilterEntity;
import com.smartwave.tools.feature.flipping.backend.services.feature.entities.UserFilterEntity;

@Component
public class FeatureDtoTransformer implements DtoTransformer<FeatureEntity, Feature> {
    
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    @Autowired
    private DtoTransformer<CanalFilterEntity, CanalFilter>         canalFilterDtoTransformer;
    
    @Autowired
    private DtoTransformer<HeaderFilterEntity, HeaderFilter>       headerFilterDtoTransformer;
    
    @Autowired
    private DtoTransformer<RoleFilterEntity, RoleFilter>           roleFilterDtoTransformer;
    
    @Autowired
    private DtoTransformer<UserFilterEntity, UserFilter>           userFilterDtoTransformer;
    
    @Autowired
    private DtoTransformer<SchedulerFilterEntity, SchedulerFilter> SchedulerFilterDtoTransformer;
    
    // =========================================================================
    // METHODS
    // =========================================================================
    @Override
    public Feature transform(FeatureEntity entiy) {
        final FeatureBuilder builder = new FeatureBuilder();
        
        builder.setUid(entiy.getUid());
        builder.setDescription(entiy.getDescription());
        builder.setTitle(entiy.getTitle());
        builder.setScreenshots(clone(entiy.getScreenshots()));
        builder.setAssets(clone(entiy.getAssets()));
        
        if (entiy.getRules() != null) {
            
            for (RuleEntity ruleEntity : entiy.getRules()) {
                builder.addRule(transformRule(ruleEntity));
            }
        }
        
        return builder.build();
    }
    
    private Rule transformRule(final RuleEntity ruleEntity) {
        final RuleBuilder ruleBuilder = new RuleBuilder();
        
        ruleBuilder.setUid(ruleEntity.getUid());
        ruleBuilder.setName(ruleEntity.getName());
        
        processSubEntryList(ruleEntity::getCanalFilters, canalFilterDtoTransformer, ruleBuilder::addCanalFilter);
        processSubEntryList(ruleEntity::getHeaderFilters, headerFilterDtoTransformer, ruleBuilder::addHeaderFilter);
        processSubEntryList(ruleEntity::getRoleFilters, roleFilterDtoTransformer, ruleBuilder::addRoleFilter);
        processSubEntryList(ruleEntity::getUserFilters, userFilterDtoTransformer, ruleBuilder::addUserFilter);
        
        if (ruleEntity.getSchedulerFilter() != null) {
            ruleBuilder.setSchedulerFilter(SchedulerFilterDtoTransformer.transform(ruleEntity.getSchedulerFilter()));
        }
        
        return ruleBuilder.build();
    }
    
    private <T, R> void processSubEntryList(Supplier<List<T>> data, DtoTransformer<T, R> transformer,
                                            Consumer<R> consumer) {
        List<T> localData = data.get();
        if (localData != null) {
            for (T entry : localData) {
                R dto = transformer.transform(entry);
                if (dto != null) {
                    consumer.accept(dto);
                }
            }
        }
    }
    
    private List<String> clone(Collection<String> values) {
        //@formatter:off
        return Optional.ofNullable(values)
                       .orElse(new ArrayList<String>())
                       .stream()
                       .collect(Collectors.toList());
        //@formatter:on
    }
}
