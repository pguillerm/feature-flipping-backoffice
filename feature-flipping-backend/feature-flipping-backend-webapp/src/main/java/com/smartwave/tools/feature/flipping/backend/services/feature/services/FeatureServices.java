package com.smartwave.tools.feature.flipping.backend.services.feature.services;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Spliterator;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.smartwave.tools.feature.flipping.backend.api.exceptions.TechnicalException;
import com.smartwave.tools.feature.flipping.backend.api.loggers.Loggers;
import com.smartwave.tools.feature.flipping.backend.api.models.DtoTransformer;
import com.smartwave.tools.feature.flipping.backend.api.models.Feature;
import com.smartwave.tools.feature.flipping.backend.services.feature.entities.FeatureEntity;
import com.smartwave.tools.feature.flipping.backend.services.feature.services.repository.CanalFilterEntityCrudRepository;
import com.smartwave.tools.feature.flipping.backend.services.feature.services.repository.FeatureCrudRepository;
import com.smartwave.tools.feature.flipping.backend.services.feature.services.repository.HeaderFilterEntityCrudRepository;
import com.smartwave.tools.feature.flipping.backend.services.feature.services.repository.RoleFilterEntityCrudRepository;
import com.smartwave.tools.feature.flipping.backend.services.feature.services.repository.RuleCrudRepository;
import com.smartwave.tools.feature.flipping.backend.services.feature.services.repository.SchedulerFilterEntityCrudRepository;
import com.smartwave.tools.feature.flipping.backend.services.feature.services.repository.UserFilterEntityCrudRepository;

@Service
public class FeatureServices {
    
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    @Autowired
    private FeatureCrudRepository                  repository;
    
    @Autowired
    private DtoTransformer<FeatureEntity, Feature> transformer;
    
    // =========================================================================
    // METHODS
    // =========================================================================
    public List<Feature> findAll() throws TechnicalException {
        final Iterable<FeatureEntity> entities = repository.findAll();
        
        final Spliterator<FeatureEntity> streamable = Optional.ofNullable(entities).orElse(Collections.EMPTY_LIST).spliterator();
        
        //@formatter:off
        return StreamSupport.stream(streamable,false)
                            .map(transformer::transform)
                            .collect(Collectors.toList());
        //@formatter:on
    }
    
    @Transactional
    public Feature findByUid(String uid) {
        Feature result = null;
        Optional<FeatureEntity> entityOpt = repository.findById(uid);
        ;
        
        if (entityOpt.isPresent()) {
            FeatureEntity entity = entityOpt.get();
            
            if (entity.getRules() != null) {
                Loggers.APPLICATION.debug("check rules : {}",entity.getRules());
            }
            result = transformer.transform(entity);
        }
        return result;
    }
    
}
