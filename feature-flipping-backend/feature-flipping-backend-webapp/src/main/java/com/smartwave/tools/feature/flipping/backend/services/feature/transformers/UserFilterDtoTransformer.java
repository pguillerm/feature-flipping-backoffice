package com.smartwave.tools.feature.flipping.backend.services.feature.transformers;

import org.springframework.stereotype.Component;

import com.smartwave.tools.feature.flipping.backend.api.models.DtoTransformer;
import com.smartwave.tools.feature.flipping.backend.api.models.rules.UserFilter;
import com.smartwave.tools.feature.flipping.backend.api.models.rules.UserFilterBuilder;
import com.smartwave.tools.feature.flipping.backend.services.feature.entities.UserFilterEntity;

@Component
public class UserFilterDtoTransformer implements DtoTransformer<UserFilterEntity, UserFilter> {
    
    // =========================================================================
    // METHODS
    // =========================================================================
    @Override
    public UserFilter transform(UserFilterEntity entity) {
        UserFilterBuilder builder = new UserFilterBuilder();
        builder.setUid(entity.getUid());
        builder.setUserName(entity.getUserName());
        return builder.build();
    }
}
