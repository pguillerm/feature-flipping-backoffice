package com.smartwave.tools.feature.flipping.backend.services.feature.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class RuleEntity {
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long                     uid;
    
    private String                   name;
    
    @OneToMany(cascade = CascadeType.ALL, targetEntity = CanalFilterEntity.class)
    private List<CanalFilterEntity>  canalFilters;
    
    @OneToOne(cascade = CascadeType.ALL, targetEntity = SchedulerFilterEntity.class)
    private SchedulerFilterEntity    schedulerFilter;
    
    @OneToMany(cascade = CascadeType.ALL, targetEntity = HeaderFilterEntity.class)
    private List<HeaderFilterEntity> headerFilters;
    
    @OneToMany(cascade = CascadeType.ALL, targetEntity = RoleFilterEntity.class)
    private List<RoleFilterEntity>   roleFilters;
    
    @OneToMany(cascade = CascadeType.ALL, targetEntity = UserFilterEntity.class)
    private List<UserFilterEntity>   userFilters;
    
    // =========================================================================
    // CONSTRUCTORS
    // =========================================================================
    public RuleEntity() {
    }
    
    public RuleEntity(Long uid) {
        super();
        this.uid = uid;
    }
    
    // =========================================================================
    // OVERRIDES
    // =========================================================================
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((uid == null) ? 0 : uid.hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        boolean result = this == obj;
        
        if (!result && obj != null && obj instanceof RuleEntity) {
            final RuleEntity other = (RuleEntity) obj;
            result = uid == null ? other.getUid() == null : uid.equals(other.getUid());
        }
        
        return result;
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("RuleEntity [uid=");
        builder.append(uid);
        builder.append(", name=");
        builder.append(name);
        builder.append(", canalFilters=");
        builder.append(canalFilters);
        builder.append(", schedulerFilter=");
        builder.append(schedulerFilter);
        builder.append(", headerFilters=");
        builder.append(headerFilters);
        builder.append(", roleFilters=");
        builder.append(roleFilters);
        builder.append(", userFilters=");
        builder.append(userFilters);
        builder.append("]");
        return builder.toString();
    }
    
    // =========================================================================
    // GETTERS & SETTERS
    // =========================================================================
    
    public Long getUid() {
        return uid;
    }
    
    public void setUid(Long uid) {
        this.uid = uid;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public List<CanalFilterEntity> getCanalFilters() {
        return canalFilters;
    }
    
    public void setCanalFilters(List<CanalFilterEntity> canalFilters) {
        this.canalFilters = canalFilters;
    }
    
    public SchedulerFilterEntity getSchedulerFilter() {
        return schedulerFilter;
    }
    
    public void setSchedulerFilter(SchedulerFilterEntity schedulerFilter) {
        this.schedulerFilter = schedulerFilter;
    }
    
    public List<HeaderFilterEntity> getHeaderFilters() {
        return headerFilters;
    }
    
    public void setHeaderFilters(List<HeaderFilterEntity> headerFilters) {
        this.headerFilters = headerFilters;
    }
    
    public List<RoleFilterEntity> getRoleFilters() {
        return roleFilters;
    }
    
    public void setRoleFilters(List<RoleFilterEntity> roleFilters) {
        this.roleFilters = roleFilters;
    }
    
    public List<UserFilterEntity> getUserFilters() {
        return userFilters;
    }
    
    public void setUserFilters(List<UserFilterEntity> userFilters) {
        this.userFilters = userFilters;
    }
    
}
