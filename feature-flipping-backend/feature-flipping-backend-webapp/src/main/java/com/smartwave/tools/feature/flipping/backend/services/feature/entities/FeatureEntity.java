package com.smartwave.tools.feature.flipping.backend.services.feature.entities;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "feature")
public class FeatureEntity {
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    @Id
    private String           uid;
    
    private String           title;
    
    private String           description;
    
    @ElementCollection(targetClass = String.class,fetch = FetchType.EAGER)
    private Set<String>      assets;
    
    @Lob
    @ElementCollection(targetClass = String.class,fetch = FetchType.EAGER)
    private List<String>     screenshots;
    
    @OneToMany(cascade = CascadeType.ALL, targetEntity = RuleEntity.class)
    private List<RuleEntity> rules;
    
    // =========================================================================
    // CONSTRUCTORS
    // =========================================================================
    public FeatureEntity() {
    }
    
    public FeatureEntity(String uid, String title, String description, Set<String> assets, String... screenshots) {
        super();
        this.uid = uid;
        this.title = title;
        this.description = description;
        this.screenshots = Arrays.asList(screenshots);
        this.assets = assets;
    }
    
    // =========================================================================
    // OVERRIDES
    // =========================================================================
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((uid == null) ? 0 : uid.hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        boolean result = this == obj;
        
        if (!result && obj != null && obj instanceof FeatureEntity) {
            final FeatureEntity other = (FeatureEntity) obj;
            result = uid == null ? other.getUid() == null : uid.equals(other.getUid());
        }
        
        return result;
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("FeatureEntity [uid=");
        builder.append(uid);
        builder.append(", title=");
        builder.append(title);
        builder.append(", description=");
        builder.append(description);
        builder.append(", screenshots=");
        builder.append(screenshots);
        builder.append(", assets=");
        builder.append(assets);
        builder.append(", rules=");
        builder.append(rules);
        builder.append("]");
        return builder.toString();
    }
    
    // =========================================================================
    // GETTERS & SETTERS
    // =========================================================================
    public String getUid() {
        return uid;
    }
    
    public void setUid(String uid) {
        this.uid = uid;
    }
    
    public String getTitle() {
        return title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public List<String> getScreenshots() {
        return screenshots;
    }
    
    public void setScreenshots(List<String> screenshots) {
        this.screenshots = screenshots;
    }
    
    public Set<String> getAssets() {
        return assets;
    }
    
    public void setAsset(Set<String> assets) {
        this.assets = assets;
    }
    
    public List<RuleEntity> getRules() {
        return rules;
    }
    
    public void setRules(List<RuleEntity> rules) {
        this.rules = rules;
    }
    
    public void setAssets(Set<String> assets) {
        this.assets = assets;
    }
    
}
