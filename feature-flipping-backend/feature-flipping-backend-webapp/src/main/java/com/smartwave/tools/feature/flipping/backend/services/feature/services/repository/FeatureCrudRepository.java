package com.smartwave.tools.feature.flipping.backend.services.feature.services.repository;

import org.springframework.data.repository.CrudRepository;

import com.smartwave.tools.feature.flipping.backend.services.feature.entities.FeatureEntity;

public interface FeatureCrudRepository extends CrudRepository<FeatureEntity, String> {

}
