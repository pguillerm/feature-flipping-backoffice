package com.smartwave.tools.feature.flipping.backend.services.feature.transformers;

import org.springframework.stereotype.Component;

import com.smartwave.tools.feature.flipping.backend.api.models.DtoTransformer;
import com.smartwave.tools.feature.flipping.backend.api.models.rules.SchedulerFilter;
import com.smartwave.tools.feature.flipping.backend.api.models.rules.SchedulerFilterBuilder;
import com.smartwave.tools.feature.flipping.backend.services.feature.entities.SchedulerFilterEntity;

@Component
public class SchedulerFilterDtoTransformer implements DtoTransformer<SchedulerFilterEntity, SchedulerFilter> {
    
    // =========================================================================
    // METHODS
    // =========================================================================
    @Override
    public SchedulerFilter transform(SchedulerFilterEntity entity) {
        SchedulerFilterBuilder builder = new SchedulerFilterBuilder();
        builder.setUid(entity.getUid());
        builder.setDuration(entity.getDuration());
        builder.setDurationUnit(entity.getDurationUnit());
        builder.setCronExpression(entity.getCronExpression());
        return builder.build();
    }
}
