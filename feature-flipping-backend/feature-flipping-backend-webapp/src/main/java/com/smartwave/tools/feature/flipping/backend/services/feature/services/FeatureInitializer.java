package com.smartwave.tools.feature.flipping.backend.services.feature.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.smartwave.tools.feature.flipping.backend.api.loggers.Loggers;
import com.smartwave.tools.feature.flipping.backend.services.feature.entities.FeatureEntity;
import com.smartwave.tools.feature.flipping.backend.services.feature.services.repository.FeatureCrudRepository;

@Component
public class FeatureInitializer {
    @Bean
    public CommandLineRunner initializeData(FeatureCrudRepository repository) {
        return arg -> {
            ObjectMapper mapper = new ObjectMapper();
            TypeReference<List<FeatureEntity>> typeReference = new TypeReference<List<FeatureEntity>>() { };
            InputStream inputStream = TypeReference.class.getResourceAsStream("/data/feature.json");
            try {
                List<FeatureEntity> data = mapper.readValue(inputStream, typeReference);
                repository.saveAll(data);
                
                Loggers.INIT.info("initialize features data");
            }
            catch (IOException e) {
                Loggers.INIT.error(e.getMessage(), e);
                Loggers.XLLOG.error(e.getMessage());
                throw e;
            }
        };
    }
}
