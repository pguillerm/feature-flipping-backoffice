package com.smartwave.tools.feature.flipping.backend.services.feature.transformers;

import org.springframework.stereotype.Component;

import com.smartwave.tools.feature.flipping.backend.api.models.DtoTransformer;
import com.smartwave.tools.feature.flipping.backend.api.models.rules.RoleFilter;
import com.smartwave.tools.feature.flipping.backend.api.models.rules.RoleFilterBuilder;
import com.smartwave.tools.feature.flipping.backend.services.feature.entities.RoleFilterEntity;

@Component
public class RoleFilterDtoTransformer implements DtoTransformer<RoleFilterEntity, RoleFilter> {
    
    // =========================================================================
    // METHODS
    // =========================================================================
    @Override
    public RoleFilter transform(RoleFilterEntity entity) {
        RoleFilterBuilder builder = new RoleFilterBuilder();
        builder.setUid(entity.getUid());
        builder.setRole(entity.getRole());
        return builder.build();
    }
}
