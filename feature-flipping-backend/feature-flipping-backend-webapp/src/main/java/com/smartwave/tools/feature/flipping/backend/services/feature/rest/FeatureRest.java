package com.smartwave.tools.feature.flipping.backend.services.feature.rest;

import java.util.List;

import javax.validation.constraints.NotBlank;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.smartwave.tools.feature.flipping.backend.api.models.Feature;
import com.smartwave.tools.feature.flipping.backend.services.feature.services.FeatureServices;

@RestController
public class FeatureRest {
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    @Autowired
    private FeatureServices featureServices;
    
    // =========================================================================
    // METHODS
    // =========================================================================
    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(path = "/feature", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Feature> findAll() throws Exception {
        return featureServices.findAll();
    }
    
    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(path = "/feature/{uid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Feature findByUid(@NotBlank @PathVariable("uid") String uid) throws Exception {
        return featureServices.findByUid(uid);
    }
    
}
