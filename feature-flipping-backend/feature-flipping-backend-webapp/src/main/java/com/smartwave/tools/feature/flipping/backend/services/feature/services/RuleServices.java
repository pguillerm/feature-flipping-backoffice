package com.smartwave.tools.feature.flipping.backend.services.feature.services;

import java.util.Optional;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smartwave.tools.feature.flipping.backend.services.feature.entities.FeatureEntity;
import com.smartwave.tools.feature.flipping.backend.services.feature.entities.RuleEntity;
import com.smartwave.tools.feature.flipping.backend.services.feature.services.repository.FeatureCrudRepository;

@Service
public class RuleServices {
    
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    @Autowired
    private FeatureCrudRepository repository;
    
    // =========================================================================
    // METHODS
    // =========================================================================
    @Transactional
    public void deleteRule(String featureUid, Long ruleUid) {
        if (ruleUid == null) {
            throw new IllegalArgumentException("ruleUid is mandatory");
        }
        
        Optional<FeatureEntity> entityOpt = repository.findById(featureUid);
        
        if (!entityOpt.isPresent()) {
            throw new EntityNotFoundException(String.format("unable to find feature with uid:%s", featureUid));
        }
        
        final FeatureEntity entity = entityOpt.get();
        
        entity.getRules().remove(new RuleEntity(ruleUid));
        
        repository.save(entity);
    }
}
