package com.smartwave.tools.feature.flipping.backend.services.feature.transformers;

import org.springframework.stereotype.Component;

import com.smartwave.tools.feature.flipping.backend.api.models.DtoTransformer;
import com.smartwave.tools.feature.flipping.backend.api.models.rules.CanalFilter;
import com.smartwave.tools.feature.flipping.backend.api.models.rules.CanalFilterBuilder;
import com.smartwave.tools.feature.flipping.backend.services.feature.entities.CanalFilterEntity;

@Component
public class CanalFilterDtoTransformer  implements DtoTransformer<CanalFilterEntity, CanalFilter>{

    
    // =========================================================================
    // METHODS
    // =========================================================================
    @Override
    public CanalFilter transform(CanalFilterEntity entity) {
        CanalFilterBuilder builder = new CanalFilterBuilder();
        builder.setUid(entity.getUid());
        builder.setCanal(entity.getCanal());
        builder.setPercent(entity.getPercent());
        return builder.build();
    }
}
