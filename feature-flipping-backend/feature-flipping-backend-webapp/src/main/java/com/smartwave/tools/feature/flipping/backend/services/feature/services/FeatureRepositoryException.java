package com.smartwave.tools.feature.flipping.backend.services.feature.services;

import com.smartwave.tools.feature.flipping.backend.exceptions.RepositoryException;

public class FeatureRepositoryException extends RepositoryException {
    
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    private static final long serialVersionUID = 5328928333631250097L;
    
    // =========================================================================
    // CONSTRUCTORS
    // =========================================================================
    public FeatureRepositoryException() {
        super();
    }
    
    public FeatureRepositoryException(String message, Object... values) {
        super(message, values);
    }
    
    public FeatureRepositoryException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public FeatureRepositoryException(String message) {
        super(message);
    }
    
    public FeatureRepositoryException(Throwable cause, String message, Object... values) {
        super(cause, message, values);
    }
    
    public FeatureRepositoryException(Throwable cause) {
        super(cause);
    }
    
}
