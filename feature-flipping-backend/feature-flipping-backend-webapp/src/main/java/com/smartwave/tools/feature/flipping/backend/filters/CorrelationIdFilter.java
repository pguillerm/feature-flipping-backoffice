package com.smartwave.tools.feature.flipping.backend.filters;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.smartwave.tools.feature.flipping.backend.api.constants.Header;
import com.smartwave.tools.feature.flipping.backend.api.loggers.Loggers;
import com.smartwave.tools.feature.flipping.backend.api.monitoring.RequestContext;
import com.smartwave.tools.feature.flipping.backend.api.monitoring.RequestInformation;
import com.smartwave.tools.feature.flipping.backend.api.monitoring.RequestInformationBuilder;


public class CorrelationIdFilter implements HandlerInterceptor {
    // =========================================================================
    // METHODS
    // =========================================================================
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
                             Object handler) throws Exception {
        final RequestInformation information = buildInformation(request);
      RequestContext.setInstance(information);
        
        return true;
    }


    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {
        HttpServletResponse httpResponse = response;
        appendResponseHeaderInformation(httpResponse);
    }

    
    
    private RequestInformation buildInformation(HttpServletRequest request) {
        final Map<String, String> header = buildHeadersMap(request);
        final RequestInformationBuilder builder = new RequestInformationBuilder();
        builder.initializeFromJvmArg();
        
        builder.setCorrelationId(buildUid(request.getHeader(Header.CORRELATION_ID)));
        builder.setRequestId(buildUid(request.getHeader(Header.REQUEST_ID)));
        builder.setConversationId(request.getHeader(Header.CONVERSATION_ID));
        //builder.setSessionId(request.getSession().getId());
        
        //builder.setService(buildUriPath(request));
        
        builder.setDeviceIdentifier(request.getHeader(Header.DEVICE_IDENTIFIER));
        builder.setDeviceType(request.getHeader(Header.DEVICE_TYPE));
        builder.setDeviceClass(request.getHeader(Header.DEVICE_CLASS));
        
        final String version = request.getHeader(Header.CLIENT_VERSION);
        builder.setVersion(request.getHeader(Header.CLIENT_VERSION));
        builder.setMajorVersion(version == null ? null : version.split("[.]")[0]);
        builder.setOsVersion(request.getHeader(Header.DEVICE_OS_VERSION));
        builder.setDeviceNetworkType(request.getHeader(Header.DEVICE_NETWORK_TYPE));
        builder.setDeviceNetworkSpeedDown(parseDouble(request.getHeader(Header.DEVICE_NETWORK_SPEED_DOWN)));
        builder.setDeviceNetworkSpeedUp(parseDouble(request.getHeader(Header.DEVICE_NETWORK_SPEED_UP)));
        builder.setDeviceNetworkSpeedLatency(parseDouble(request.getHeader(Header.DEVICE_NETWORK_SPEED_LATENCY)));
        
        //builder.setRemoteAddress(request.getRemoteAddr());
        builder.setDeviceIp(request.getHeader(Header.DEVICE_IP));
        builder.setUserAgent(request.getHeader(Header.USER_AGENT));
        builder.setLanguage(request.getHeader(Header.LANGUAGE));
        builder.setCountry(request.getHeader(Header.COUNTRY));
        
        return builder.build();
    }
    
    private void appendResponseHeaderInformation(final HttpServletResponse httpResponse) {
        final RequestInformation requestContext = RequestContext.getInstance();
        httpResponse.setHeader(Header.CORRELATION_ID, requestContext.getCorrelationId());
        httpResponse.setHeader(Header.CORRELATION_ID, requestContext.getRequestId());
    }
    
    // =========================================================================
    // TOOLS
    // =========================================================================
    public static Map<String, String> buildHeadersMap(final HttpServletRequest request) {
        final Map<String, String> header = new HashMap<>();
        Enumeration<String> names = request.getHeaderNames();
        while (names.hasMoreElements()) {
            final String key = names.nextElement();
            header.put(key.toLowerCase(), request.getHeader(key));
        }
        return header;
    }
    
    private String buildUid(final String uid) {
        return (uid == null) || uid.trim().isEmpty() ? UUID.randomUUID().toString() : uid;
    }
    
    private String buildUriPath(final HttpServletRequest request) {
        final String contextPath = request.getContextPath();
        final String path = request.getRequestURI().toString();
        return path.length() >= contextPath.length() ? path.substring(contextPath.length()) : path;
    }
    
    private static Double parseDouble(final String value) {
        Double result = null;
        if (value != null) {
            try {
                result = Double.parseDouble(value);
            }
            catch (final Exception e) {
                Loggers.DEBUG.error(e.getMessage(), e);
            }
        }
        return result;
    }
    
}
