package com.smartwave.tools.feature.flipping.backend.services.feature.services.repository;

import org.springframework.data.repository.CrudRepository;

import com.smartwave.tools.feature.flipping.backend.services.feature.entities.RuleEntity;

public interface RuleCrudRepository extends CrudRepository<RuleEntity, Long> {
    
}
