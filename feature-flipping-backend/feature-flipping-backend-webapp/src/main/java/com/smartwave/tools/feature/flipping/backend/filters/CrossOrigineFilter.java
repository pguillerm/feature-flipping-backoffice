package com.smartwave.tools.feature.flipping.backend.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;

@Component
public class CrossOrigineFilter implements Filter {
    private static final String HEADER = "Accept, x-device-identifier,x-correlation-id,x-conversation-id,x-client-version,x-device-type,x-device-class,x-device-os-version,x-device-version,x-device-network-type,x-device-network-speed-down,x-device-network-speed-up,x-device-network-speed-latency,x-language,x-country,x-device-ip";
    
    // =========================================================================
    // METHODS
    // =========================================================================
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
                                                                                              ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest)request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        
        httpServletResponse.setHeader("Access-Control-Allow-Origin", "*");
        httpServletResponse.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, PATCH, OPTIONS");
        
        
        httpServletResponse.setHeader("Access-Control-Allow-Headers", HEADER);
        httpServletResponse.setHeader("Access-Control-Expose-Headers",HEADER);
        
        
        chain.doFilter(request, response);
    }
}
