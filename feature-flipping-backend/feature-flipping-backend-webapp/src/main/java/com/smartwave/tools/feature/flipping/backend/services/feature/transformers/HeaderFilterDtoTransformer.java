package com.smartwave.tools.feature.flipping.backend.services.feature.transformers;

import org.springframework.stereotype.Component;

import com.smartwave.tools.feature.flipping.backend.api.models.DtoTransformer;
import com.smartwave.tools.feature.flipping.backend.api.models.rules.HeaderFilter;
import com.smartwave.tools.feature.flipping.backend.api.models.rules.HeaderFilterBuilder;
import com.smartwave.tools.feature.flipping.backend.services.feature.entities.HeaderFilterEntity;

@Component
public class HeaderFilterDtoTransformer implements DtoTransformer<HeaderFilterEntity, HeaderFilter> {
    
    // =========================================================================
    // METHODS
    // =========================================================================
    @Override
    public HeaderFilter transform(HeaderFilterEntity entity) {
        final HeaderFilterBuilder builder = new HeaderFilterBuilder();
        builder.setUid(entity.getUid());
        builder.setName(entity.getName());
        builder.setValue(entity.getValue());
        
        return builder.build();
    }
}
