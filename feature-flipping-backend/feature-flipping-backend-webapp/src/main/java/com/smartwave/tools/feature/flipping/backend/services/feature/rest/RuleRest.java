package com.smartwave.tools.feature.flipping.backend.services.feature.rest;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.smartwave.tools.feature.flipping.backend.services.feature.services.RuleServices;

@RestController
public class RuleRest {
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    @Autowired
    private RuleServices ruleServices;
    
    // =========================================================================
    // METHODS
    // =========================================================================
    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(path = "/rule/{featureUid}/{ruleUid}",
                    method = RequestMethod.DELETE,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteRule(@NotBlank @PathVariable("featureUid") String featureUid,
                           @NotNull @PathVariable("ruleUid") long ruleUid) {
        ruleServices.deleteRule(featureUid, ruleUid);
    }
}
