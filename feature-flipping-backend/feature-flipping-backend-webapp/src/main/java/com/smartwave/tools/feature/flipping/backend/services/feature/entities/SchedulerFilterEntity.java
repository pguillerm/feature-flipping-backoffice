package com.smartwave.tools.feature.flipping.backend.services.feature.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class SchedulerFilterEntity {
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long   uid;
    
    private Long   duration;
    
    private String durationUnit;
    
    private String cronExpression;
    
    // =========================================================================
    // OVERRIDES
    // =========================================================================
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((uid == null) ? 0 : uid.hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        boolean result = this == obj;
        
        if (!result && obj != null && obj instanceof SchedulerFilterEntity) {
            final SchedulerFilterEntity other = (SchedulerFilterEntity) obj;
            result = uid == null ? other.getUid() == null : uid.equals(other.getUid());
        }
        
        return result;
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SchedulerFilterEntity [uid=");
        builder.append(uid);
        builder.append(", duration=");
        builder.append(duration);
        builder.append(", durationUnit=");
        builder.append(durationUnit);
        builder.append(", cronExpression=");
        builder.append(cronExpression);
        builder.append("]");
        return builder.toString();
    }
    // =========================================================================
    // GETTERS & SETTERS
    // =========================================================================
    
    public Long getUid() {
        return uid;
    }
    
    public void setUid(Long uid) {
        this.uid = uid;
    }
    
    public Long getDuration() {
        return duration;
    }
    
    public void setDuration(Long duration) {
        this.duration = duration;
    }
    
    public String getDurationUnit() {
        return durationUnit;
    }
    
    public void setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
    }
    
    public String getCronExpression() {
        return cronExpression;
    }
    
    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }
}
