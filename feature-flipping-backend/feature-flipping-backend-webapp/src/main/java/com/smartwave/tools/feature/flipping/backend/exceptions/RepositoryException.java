package com.smartwave.tools.feature.flipping.backend.exceptions;

import com.smartwave.tools.feature.flipping.backend.api.exceptions.TechnicalException;

public class RepositoryException extends TechnicalException {
    
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    private static final long serialVersionUID = -5949453324679497560L;
    
    // =========================================================================
    // CONSTRUCTORS
    // =========================================================================
    public RepositoryException() {
        super();
    }
    
    public RepositoryException(String message, Object... values) {
        super(message, values);
    }
    
    public RepositoryException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public RepositoryException(String message) {
        super(message);
    }
    
    public RepositoryException(Throwable cause, String message, Object... values) {
        super(cause, message, values);
    }
    
    public RepositoryException(Throwable cause) {
        super(cause);
    }
    
}
