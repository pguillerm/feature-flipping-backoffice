package com.smartwave.tools.feature.flipping.backend.api.models.rules;

import java.io.Serializable;

public class RoleFilter implements Serializable {
    
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    private static final long serialVersionUID = -8249121375097906435L;
    
    private final Long        uid;
    
    private final String      role;
    
    // =========================================================================
    // CONSTRUCTORS
    // =========================================================================
    /* package */ RoleFilter(Long uid, String role) {
        super();
        this.uid = uid;
        this.role = role;
    }
    
    // =========================================================================
    // OVERRIDES
    // =========================================================================
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((uid == null) ? 0 : uid.hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        boolean result = this == obj;
        
        if (!result && obj != null && obj instanceof RoleFilter) {
            final RoleFilter other = (RoleFilter) obj;
            result = uid == null ? other.getUid() == null : uid.equals(other.getUid());
        }
        
        return result;
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("RoleFilter [uid=");
        builder.append(uid);
        builder.append(", role=");
        builder.append(role);
        builder.append("]");
        return builder.toString();
    }
    // =========================================================================
    // GETTERS & SETTERS
    // =========================================================================
    
    public Long getUid() {
        return uid;
    }
    
    public String getRole() {
        return role;
    }
    
}
