package com.smartwave.tools.feature.flipping.backend.api.models.rules;

import java.io.Serializable;

public class UserFilter implements Serializable {
    
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    private static final long serialVersionUID = -8349637155290066270L;
    
    private final Long        uid;
    
    private final String      userName;
    
    // =========================================================================
    // CONSTRUCTORS
    // =========================================================================
    /* package */ UserFilter(Long uid, String userName) {
        super();
        this.uid = uid;
        this.userName = userName;
    }
    
    // =========================================================================
    // OVERRIDES
    // =========================================================================
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((uid == null) ? 0 : uid.hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        boolean result = this == obj;
        
        if (!result && obj != null && obj instanceof UserFilter) {
            final UserFilter other = (UserFilter) obj;
            result = uid == null ? other.getUid() == null : uid.equals(other.getUid());
        }
        
        return result;
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("UserFilter [uid=");
        builder.append(uid);
        builder.append(", userName=");
        builder.append(userName);
        builder.append("]");
        return builder.toString();
    }
    
    // =========================================================================
    // GETTERS & SETTERS
    // =========================================================================
    public Long getUid() {
        return uid;
    }
    
    public String getUserName() {
        return userName;
    }
    
}
