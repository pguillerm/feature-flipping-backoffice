package com.smartwave.tools.feature.flipping.backend.api.models.rules;

import com.smartwave.tools.feature.flipping.backend.api.models.Builder;

public class SchedulerFilterBuilder implements Builder<SchedulerFilter> {
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    private Long   uid;
    
    private Long   duration;
    
    private String durationUnit;
    
    private String cronExpression;
    
    // =========================================================================
    // CONSTRUCTORS
    // =========================================================================
    public SchedulerFilterBuilder() {
    }
    
    public SchedulerFilterBuilder(Long uid, Long duration, String durationUnit, String cronExpression) {
        super();
        this.uid = uid;
        this.duration = duration;
        this.durationUnit = durationUnit;
        this.cronExpression = cronExpression;
    }
    
    @Override
    public SchedulerFilter build() {
        return new SchedulerFilter(uid, duration, durationUnit, cronExpression);
    }
    
    @Override
    public SchedulerFilterBuilder clear() {
        uid = null;
        duration = null;
        durationUnit = null;
        cronExpression = null;
        return this;
    }
    
    // =========================================================================
    // OVERRIDES
    // =========================================================================
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((uid == null) ? 0 : uid.hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        boolean result = this == obj;
        
        if (!result && obj != null && obj instanceof SchedulerFilterBuilder) {
            final SchedulerFilterBuilder other = (SchedulerFilterBuilder) obj;
            result = uid == null ? other.getUid() == null : uid.equals(other.getUid());
        }
        
        return result;
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SchedulerFilter [uid=");
        builder.append(uid);
        builder.append(", duration=");
        builder.append(duration);
        builder.append(", durationUnit=");
        builder.append(durationUnit);
        builder.append(", cronExpression=");
        builder.append(cronExpression);
        builder.append("]");
        return builder.toString();
    }
    
    // =========================================================================
    // GETTERS & SETTERS
    // =========================================================================
    public Long getUid() {
        return uid;
    }
    
    public SchedulerFilterBuilder setUid(Long uid) {
        this.uid = uid;
        return this;
    }
    
    public Long getDuration() {
        return duration;
    }
    
    public SchedulerFilterBuilder setDuration(Long duration) {
        this.duration = duration;
        return this;
    }
    
    public String getDurationUnit() {
        return durationUnit;
    }
    
    public SchedulerFilterBuilder setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
        return this;
    }
    
    public String getCronExpression() {
        return cronExpression;
    }
    
    public SchedulerFilterBuilder setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
        return this;
    }
    
}
