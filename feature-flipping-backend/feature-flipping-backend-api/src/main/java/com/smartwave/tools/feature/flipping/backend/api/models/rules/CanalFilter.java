package com.smartwave.tools.feature.flipping.backend.api.models.rules;

import java.io.Serializable;

public class CanalFilter implements Serializable {
    
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    private static final long serialVersionUID = -7970457104252309316L;
    
    private final Long        uid;
    
    private final String      canal;
    
    private final Double      percent;
    
    // =========================================================================
    // CONSTRUCTORS
    // =========================================================================
    /* package */ CanalFilter(Long uid, String canal, Double percent) {
        super();
        this.uid = uid;
        this.canal = canal;
        this.percent = percent;
    }
    
    // =========================================================================
    // OVERRIDES
    // =========================================================================
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((uid == null) ? 0 : uid.hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        boolean result = this == obj;
        
        if (!result && obj != null && obj instanceof CanalFilter) {
            final CanalFilter other = (CanalFilter) obj;
            result = uid == null ? other.getUid() == null : uid.equals(other.getUid());
        }
        
        return result;
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("CanalFilter [uid=");
        builder.append(uid);
        builder.append(", canal=");
        builder.append(canal);
        builder.append(", percent=");
        builder.append(percent);
        builder.append("]");
        return builder.toString();
    }
    
    // =========================================================================
    // GETTERS & SETTERS
    // =========================================================================
    public Long getUid() {
        return uid;
    }
    
    public String getCanal() {
        return canal;
    }
    
    public Double getPercent() {
        return percent;
    }
    
}
