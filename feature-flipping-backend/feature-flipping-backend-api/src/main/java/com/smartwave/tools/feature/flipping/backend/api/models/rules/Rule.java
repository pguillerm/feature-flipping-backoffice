package com.smartwave.tools.feature.flipping.backend.api.models.rules;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

public class Rule implements Serializable {
    
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    private static final long        serialVersionUID = -4389964123810965834L;
    
    private final Long               uid;
    
    private final String             name;
    
    private final List<CanalFilter>  canalFilters;
    
    private final SchedulerFilter    schedulerFilter;
    
    private final List<HeaderFilter> headerFilters;
    
    private final List<RoleFilter>   roleFilters;
    
    private final List<UserFilter>   userFilters;
    
    // =========================================================================
    // CONSTRUCTORS
    // =========================================================================
    /* package */ Rule(Long uid, String name, List<CanalFilter> canalFilters, SchedulerFilter schedulerFilter,
                       List<HeaderFilter> headerFilters, List<RoleFilter> roleFilters, List<UserFilter> userFilters) {
        super();
        this.uid = uid;
        this.name = name;
        this.canalFilters = canalFilters;
        this.schedulerFilter = schedulerFilter;
        this.headerFilters = headerFilters;
        this.roleFilters = roleFilters;
        this.userFilters = userFilters;
    }
    
    // =========================================================================
    // OVERRIDES
    // =========================================================================
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((uid == null) ? 0 : uid.hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        boolean result = this == obj;
        
        if (!result && obj != null && obj instanceof Rule) {
            final Rule other = (Rule) obj;
            result = uid == null ? other.getUid() == null : uid.equals(other.getUid());
        }
        
        return result;
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Rule [uid=");
        builder.append(uid);
        builder.append(", name=");
        builder.append(name);
        builder.append(", canalFilters=");
        builder.append(canalFilters);
        builder.append(", schedulerFilter=");
        builder.append(schedulerFilter);
        builder.append(", headerFilters=");
        builder.append(headerFilters);
        builder.append(", roleFilters=");
        builder.append(roleFilters);
        builder.append(", userFilters=");
        builder.append(userFilters);
        builder.append("]");
        return builder.toString();
    }
    
    // =========================================================================
    // GETTERS & SETTERS
    // =========================================================================
    public Long getUid() {
        return uid;
    }
    
    public String getName() {
        return name;
    }
    
    public List<CanalFilter> getCanalFilters() {
        return canalFilters == null ? null : Collections.unmodifiableList(canalFilters);
    }
    
    public SchedulerFilter getSchedulerFilter() {
        return schedulerFilter;
    }
    
    public List<HeaderFilter> getHeaderFilters() {
        return headerFilters == null ? null : Collections.unmodifiableList(headerFilters);
    }
    
    public List<RoleFilter> getRoleFilters() {
        return roleFilters == null ? null : Collections.unmodifiableList(roleFilters);
    }
    
    public List<UserFilter> getUserFilters() {
        return userFilters == null ? null : Collections.unmodifiableList(userFilters);
    }
}
