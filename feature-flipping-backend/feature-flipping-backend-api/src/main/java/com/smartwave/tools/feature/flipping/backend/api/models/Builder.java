package com.smartwave.tools.feature.flipping.backend.api.models;

import java.util.ArrayList;
import java.util.List;

public interface Builder<T> {
    T build();
    
    <B extends Builder<T>> B clear();
    
    default <D> List<D> cloneList(List<D> ref) {
        return ref == null ? null : new ArrayList<>(ref);
    }
}
