package com.smartwave.tools.feature.flipping.backend.api.exceptions;

public class TechnicalException extends Exception {
    
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 389031756408740003L;
    
    public static final int   ERR_CODE         = 500;
    
    private final int         code;
    
    // =========================================================================
    // PROTECTED CONSTRUCTORS
    // =========================================================================
    protected TechnicalException(final int code, final String message, final Throwable cause) {
        super(message, cause);
        this.code = code;
    }
    
    // =========================================================================
    // PUBLIC CONSTRUCTORS
    // =========================================================================
    
    public TechnicalException() {
        this(ERR_CODE, null, null);
    }
    
    public TechnicalException(final String message, final Throwable cause) {
        this(ERR_CODE, message, cause);
    }
    
    public TechnicalException(final String message, final Object... values) {
        this(ERR_CODE, MessagesFormatter.format(message, values), null);
    }
    
    public TechnicalException(final Throwable cause, final String message, final Object... values) {
        this(ERR_CODE, MessagesFormatter.format(message, values), cause);
    }
    
    public TechnicalException(final String message) {
        this(ERR_CODE, message, null);
    }
    
    public TechnicalException(final Throwable cause) {
        this(ERR_CODE, null, cause);
    }
    
    // =========================================================================
    // CODE
    // =========================================================================
    public int getCode() {
        return code;
    }
    
}
