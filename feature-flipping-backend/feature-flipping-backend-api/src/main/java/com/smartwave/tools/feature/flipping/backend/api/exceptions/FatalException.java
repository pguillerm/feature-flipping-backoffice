package com.smartwave.tools.feature.flipping.backend.api.exceptions;

public class FatalException extends RuntimeException {
    
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -7204197977074790471L;
    
    // =========================================================================
    // CONSTRUCTORS
    // =========================================================================
    public FatalException() {
        super();
    }
    
    public FatalException(final String message, final Throwable cause) {
        super(message, cause);
    }
    
    public FatalException(final String message) {
        super(message);
    }
    
    public FatalException(final Throwable cause) {
        super(cause);
    }
    
    public FatalException(final String message, final Object... values) {
        super(MessagesFormatter.format(message, values));
    }
    
    public FatalException(final Throwable cause, final String message, final Object... values) {
        super(MessagesFormatter.format(message, values), cause);
    }
    
}
