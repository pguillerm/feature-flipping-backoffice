package com.smartwave.tools.feature.flipping.backend.api.models.rules;

import com.smartwave.tools.feature.flipping.backend.api.models.Builder;

public class RoleFilterBuilder implements Builder<RoleFilter> {
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    private Long   uid;
    
    private String role;
    
    // =========================================================================
    // CONSTRUCTORS
    // =========================================================================
    public RoleFilterBuilder() {
    }
    
    public RoleFilterBuilder(Long uid, String role) {
        super();
        this.uid = uid;
        this.role = role;
    }
    
    @Override
    public RoleFilter build() {
        return new RoleFilter(uid, role);
    }
    
    @Override
    public RoleFilterBuilder clear() {
        uid = null;
        role = null;
        return this;
    }
    
    // =========================================================================
    // OVERRIDES
    // =========================================================================
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((uid == null) ? 0 : uid.hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        boolean result = this == obj;
        
        if (!result && obj != null && obj instanceof RoleFilterBuilder) {
            final RoleFilterBuilder other = (RoleFilterBuilder) obj;
            result = uid == null ? other.getUid() == null : uid.equals(other.getUid());
        }
        
        return result;
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("RoleFilter [uid=");
        builder.append(uid);
        builder.append(", role=");
        builder.append(role);
        builder.append("]");
        return builder.toString();
    }
    // =========================================================================
    // GETTERS & SETTERS
    // =========================================================================
    
    public Long getUid() {
        return uid;
    }
    
    public RoleFilterBuilder setUid(Long uid) {
        this.uid = uid;
        return this;
    }
    
    public String getRole() {
        return role;
    }
    
    public RoleFilterBuilder setRole(String role) {
        this.role = role;
        return this;
    }
    
}
