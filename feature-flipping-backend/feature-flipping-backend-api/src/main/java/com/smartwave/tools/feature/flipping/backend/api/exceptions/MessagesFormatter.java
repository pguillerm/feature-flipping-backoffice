package com.smartwave.tools.feature.flipping.backend.api.exceptions;

import java.text.MessageFormat;
import java.util.regex.Pattern;

public class MessagesFormatter {
    
    // =========================================================================
    // CONSTRUCTORS
    // =========================================================================
    private final static Pattern REGEX = Pattern.compile("[{][^}]+[}]");
    
    private MessagesFormatter() {
        
    }
    
    // =========================================================================
    // METHODS
    // =========================================================================
    public static String format(final String format, final Object... values) {
        String result = null;
        if (format != null) {
            if ((values == null) || (values.length == 0)) {
                result = format;
            }
            else if (REGEX.matcher(format).find()) {
                final MessageFormat formater = new MessageFormat(format.replaceAll("'", "''"));
                result = formater.format(values);
            }
            else {
                result = format;
            }
        }
        return result;
    }
}
