package com.smartwave.tools.feature.flipping.backend.api.models.rules;

import java.io.Serializable;

public class SchedulerFilter implements Serializable {
    
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    private static final long serialVersionUID = -146216430330756754L;
    
    private final Long        uid;
    
    private final Long        duration;
    
    private final String      durationUnit;
    
    private final String      cronExpression;
    
    // =========================================================================
    // CONSTRUCTORS
    // =========================================================================
    /* package */ SchedulerFilter(Long uid, Long duration, String durationUnit, String cronExpression) {
        super();
        this.uid = uid;
        this.duration = duration;
        this.durationUnit = durationUnit;
        this.cronExpression = cronExpression;
    }
    
    // =========================================================================
    // OVERRIDES
    // =========================================================================
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((uid == null) ? 0 : uid.hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        boolean result = this == obj;
        
        if (!result && obj != null && obj instanceof SchedulerFilter) {
            final SchedulerFilter other = (SchedulerFilter) obj;
            result = uid == null ? other.getUid() == null : uid.equals(other.getUid());
        }
        
        return result;
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SchedulerFilter [uid=");
        builder.append(uid);
        builder.append(", duration=");
        builder.append(duration);
        builder.append(", durationUnit=");
        builder.append(durationUnit);
        builder.append(", cronExpression=");
        builder.append(cronExpression);
        builder.append("]");
        return builder.toString();
    }
    
    // =========================================================================
    // GETTERS & SETTERS
    // =========================================================================
    public Long getUid() {
        return uid;
    }
    
    public Long getDuration() {
        return duration;
    }
    
    public String getDurationUnit() {
        return durationUnit;
    }
    
    public String getCronExpression() {
        return cronExpression;
    }
    
}
