package com.smartwave.tools.feature.flipping.backend.api.models;

import java.util.ArrayList;
import java.util.List;

import com.smartwave.tools.feature.flipping.backend.api.models.rules.Rule;

public class FeatureBuilder implements Builder<Feature> {
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    private String       uid;
    
    private String       title;
    
    private String       description;
    
    private List<String> screenshots;
    
    private List<String> assets;
    
    private List<Rule>   rules;
    
    // =========================================================================
    // METHODS
    // =========================================================================
    public Feature build() {
        return new Feature(uid, title, description, cloneList(screenshots), cloneList(assets), cloneList(rules));
    }
    
    // =========================================================================
    // OVERRIDES
    // =========================================================================
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("FeatureBuilder [uid=");
        builder.append(uid);
        builder.append(", title=");
        builder.append(title);
        builder.append(", description=");
        builder.append(description);
        builder.append(", screenshots=");
        builder.append(screenshots);
        builder.append(", asset=");
        builder.append(assets);
        builder.append("]");
        return builder.toString();
    }
    
    @Override
    public FeatureBuilder clear() {
        uid = null;
        title = null;
        description = null;
        screenshots = null;
        assets = null;
        return this;
    }
    
    // =========================================================================
    // GETTERS & SETTERS
    // =========================================================================
    public String getUid() {
        return uid;
    }
    
    public FeatureBuilder setUid(String uid) {
        this.uid = uid;
        return this;
    }
    
    public String getTitle() {
        return title;
    }
    
    public FeatureBuilder setTitle(String title) {
        this.title = title;
        return this;
    }
    
    public String getDescription() {
        return description;
    }
    
    public FeatureBuilder setDescription(String description) {
        this.description = description;
        return this;
    }
    
    public List<String> getScreenshots() {
        return screenshots;
    }
    
    public FeatureBuilder setScreenshots(List<String> screenshots) {
        this.screenshots = screenshots;
        return this;
    }
    
    public FeatureBuilder addScreenshots(String screenshot) {
        if (screenshots == null) {
            screenshots = new ArrayList<>();
        }
        if (screenshot != null) {
            screenshots.add(screenshot);
        }
        return this;
    }
    
    public List<String> getAssets() {
        return assets;
    }
    
    public FeatureBuilder setAssets(List<String> asset) {
        this.assets = asset;
        return this;
    }
    
    public FeatureBuilder addAsset(String asset) {
        if (this.assets == null) {
            this.assets = new ArrayList<>();
        }
        
        if (asset != null) {
            this.assets.add(asset);
        }
        return this;
    }
    
    public List<Rule> getRules() {
        return rules;
    }
    
    public FeatureBuilder setRules(List<Rule> rules) {
        this.rules = rules;
        return this;
    }
    
    public FeatureBuilder addRule(Rule rule) {
        if (rules == null) {
            rules = new ArrayList<Rule>();
        }
        if (rule != null) {
            rules.add(rule);
        }
        
        return this;
    }
    
}
