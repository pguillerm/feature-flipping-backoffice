package com.smartwave.tools.feature.flipping.backend.api.models.rules;

import java.io.Serializable;

public class HeaderFilter implements Serializable {
    
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    private static final long serialVersionUID = -8463714126089108333L;
    
    private final Long        uid;
    
    private final String      name;
    
    private final String      value;
    
    // =========================================================================
    // CONSTRUCTORS
    // =========================================================================
    /* package */ HeaderFilter(final Long uid, final String name, final String value) {
        super();
        this.uid = uid;
        this.name = name;
        this.value = value;
    }
    
    // =========================================================================
    // OVERRIDES
    // =========================================================================
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((uid == null) ? 0 : uid.hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        boolean result = this == obj;
        
        if (!result && obj != null && obj instanceof HeaderFilter) {
            final HeaderFilter other = (HeaderFilter) obj;
            result = uid == null ? other.getUid() == null : uid.equals(other.getUid());
        }
        
        return result;
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("HeaderFilter [uid=");
        builder.append(uid);
        builder.append(", name=");
        builder.append(name);
        builder.append(", value=");
        builder.append(value);
        builder.append("]");
        return builder.toString();
    }
    
    // =========================================================================
    // GETTERS & SETTERS
    // =========================================================================
    
    public Long getUid() {
        return uid;
    }
    
    public String getName() {
        return name;
    }
    
    public String getValue() {
        return value;
    }
}
