package com.smartwave.tools.feature.flipping.backend.api.monitoring;

import java.util.UUID;

public class RequestContext {
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    private static ThreadLocal<RequestInformation> INSTANCE = new ThreadLocal<>();
    
    // =========================================================================
    // CONSTRUCTORS
    // =========================================================================
    public static RequestInformation getInstance() {
        RequestInformation result = INSTANCE.get();
        if (result == null) {
            result = initializeTechnicalRequest();
        }
        return result;
    }
    public static synchronized void setInstance(final RequestInformation instance) {
        INSTANCE.set(instance);
        MdcService.initialize();
    }
    // =========================================================================
    // METHODS
    // =========================================================================
    private static RequestInformation initializeTechnicalRequest() {
        final RequestInformationBuilder builder = new RequestInformationBuilder();
        builder.initializeFromJvmArg();
        
        builder.setDeviceIdentifier("system");
        builder.setCorrelationId(UUID.randomUUID().toString());
        builder.setRequestId(UUID.randomUUID().toString());
        builder.setService(String.join("_", "technical", Thread.currentThread().getName()));
        
        final RequestInformation result = builder.build();
        setInstance(result);
        return result;
    }
}
