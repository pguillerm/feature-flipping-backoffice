package com.smartwave.tools.feature.flipping.backend.api.models.rules;

import com.smartwave.tools.feature.flipping.backend.api.models.Builder;

public class UserFilterBuilder implements Builder<UserFilter> {
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    private Long   uid;
    
    private String userName;
    
    // =========================================================================
    // CONSTRUCTORS
    // =========================================================================
    public UserFilterBuilder() {
    }
    
    public UserFilterBuilder(Long uid, String userName) {
        
        super();
        this.uid = uid;
        this.userName = userName;
    }
    
    @Override
    public UserFilter build() {
        return new UserFilter(uid, userName);
    }
    
    @Override
    public UserFilterBuilder clear() {
        uid = null;
        userName = null;
        return this;
    }
    
    // =========================================================================
    // OVERRIDES
    // =========================================================================
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((uid == null) ? 0 : uid.hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        boolean result = this == obj;
        
        if (!result && obj != null && obj instanceof UserFilterBuilder) {
            final UserFilterBuilder other = (UserFilterBuilder) obj;
            result = uid == null ? other.getUid() == null : uid.equals(other.getUid());
        }
        
        return result;
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("UserFilter [uid=");
        builder.append(uid);
        builder.append(", userName=");
        builder.append(userName);
        builder.append("]");
        return builder.toString();
    }
    
    // =========================================================================
    // GETTERS & SETTERS
    // =========================================================================
    public Long getUid() {
        return uid;
    }
    
    public UserFilterBuilder setUid(Long uid) {
        this.uid = uid;
        return this;
    }
    
    public String getUserName() {
        return userName;
    }
    
    public UserFilterBuilder setUserName(String userName) {
        this.userName = userName;
        return this;
    }
    
}
