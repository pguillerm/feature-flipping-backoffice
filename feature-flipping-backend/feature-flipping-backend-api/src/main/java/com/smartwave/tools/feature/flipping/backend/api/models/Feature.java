package com.smartwave.tools.feature.flipping.backend.api.models;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import com.smartwave.tools.feature.flipping.backend.api.models.rules.Rule;

public class Feature implements Serializable {
    
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    private static final long  serialVersionUID = 4021057378597086140L;
    
    private final String       uid;
    
    private final String       title;
    
    private final String       description;
    
    private final List<String> assets;
    
    private final List<String> screenshots;
    
    private final List<Rule>   rules;
    
    // =========================================================================
    // CONSTRUCTORS
    // =========================================================================
    /* package */ Feature(String uid, String title, String description, List<String> screenshots, List<String> assets,
                          List<Rule> rules) {
        super();
        this.uid = uid;
        this.title = title;
        this.description = description;
        this.screenshots = screenshots;
        this.assets = assets;
        this.rules = rules;
    }
    
    // =========================================================================
    // OVERRIDES
    // =========================================================================
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((uid == null) ? 0 : uid.hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        boolean result = this == obj;
        
        if (!result && obj != null && obj instanceof Feature) {
            final Feature other = (Feature) obj;
            result = uid == null ? other.getUid() == null : uid.equals(other.getUid());
        }
        
        return result;
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Feature [uid=");
        builder.append(uid);
        builder.append(", title=");
        builder.append(title);
        builder.append(", description=");
        builder.append(description);
        builder.append(", screenshots=");
        builder.append(screenshots);
        builder.append(", rules=");
        builder.append(rules);
        builder.append("]");
        return builder.toString();
    }
    
    // =========================================================================
    // GETTERS & SETTERS
    // =========================================================================
    
    public String getUid() {
        return uid;
    }
    
    public String getTitle() {
        return title;
    }
    
    public String getDescription() {
        return description;
    }
    
    public List<String> getScreenshots() {
        return screenshots == null ? null : Collections.unmodifiableList(screenshots);
    }
    
    public List<String> getAssets() {
        return assets == null ? null : Collections.unmodifiableList(assets);
    }
    
    public List<Rule> getRules() {
        return rules == null ? null : Collections.unmodifiableList(rules);
    }
    
}
