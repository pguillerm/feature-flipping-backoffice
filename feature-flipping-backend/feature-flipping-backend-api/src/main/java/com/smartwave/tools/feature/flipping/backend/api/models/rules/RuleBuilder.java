package com.smartwave.tools.feature.flipping.backend.api.models.rules;

import java.util.ArrayList;
import java.util.List;

import com.smartwave.tools.feature.flipping.backend.api.models.Builder;

public class RuleBuilder implements Builder<Rule> {
    
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    private Long               uid;
    
    private String             name;
    
    private List<CanalFilter>  canalFilters;
    
    private SchedulerFilter    schedulerFilter;
    
    private List<HeaderFilter> headerFilters;
    
    private List<RoleFilter>   roleFilters;
    
    private List<UserFilter>   userFilters;
    
  
    
    // =========================================================================
    // CONSTRUCTORS
    // =========================================================================
    @Override
    public Rule build() {
        return new Rule(uid, name, canalFilters, schedulerFilter, headerFilters, roleFilters, userFilters);
    }
    
    @Override
    public RuleBuilder clear() {
        uid = null;
        name = null;
        canalFilters = null;
        schedulerFilter = null;
        headerFilters = null;
        roleFilters = null;
        userFilters = null;
        return this;
    }
    
    // =========================================================================
    // OVERRIDES
    // =========================================================================
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("RuleBuilder [uid=");
        builder.append(uid);
        builder.append(", name=");
        builder.append(name);
        builder.append(", canalFilters=");
        builder.append(canalFilters);
        builder.append(", schedulerFilter=");
        builder.append(schedulerFilter);
        builder.append(", headerFilters=");
        builder.append(headerFilters);
        builder.append(", roleFilters=");
        builder.append(roleFilters);
        builder.append(", userFilters=");
        builder.append(userFilters);
        builder.append("]");
        return builder.toString();
    }
    
    // =========================================================================
    // GETTERS & SETTERS
    // =========================================================================
    public long getUid() {
        return uid;
    }
    
    public RuleBuilder setUid(long uid) {
        this.uid = uid;
        return this;
    }
    
    public String getName() {
        return name;
    }
    
    public RuleBuilder setName(String name) {
        this.name = name;
        return this;
    }
    
    public List<CanalFilter> getCanalFilters() {
        return canalFilters;
    }
    
    public RuleBuilder setCanalFilters(List<CanalFilter> canalFilters) {
        this.canalFilters = canalFilters;
        return this;
    }
    
    public RuleBuilder addCanalFilter(CanalFilter canalFilter) {
        if (canalFilters == null) {
            canalFilters = new ArrayList<>();
        }
        if (canalFilter != null) {
            canalFilters.add(canalFilter);
        }
        return this;
    }
    
    public SchedulerFilter getSchedulerFilter() {
        return schedulerFilter;
    }
    
    public RuleBuilder setSchedulerFilter(SchedulerFilter schedulerFilter) {
        this.schedulerFilter = schedulerFilter;
        return this;
    }
    
    public List<HeaderFilter> getHeaderFilters() {
        return headerFilters;
    }
    
    public RuleBuilder setHeaderFilters(List<HeaderFilter> headerFilters) {
        this.headerFilters = headerFilters;
        return this;
    }
    
    public RuleBuilder addHeaderFilter(HeaderFilter headerFilter) {
        if (headerFilters == null) {
            headerFilters = new ArrayList<>();
        }
        if (headerFilter != null) {
            headerFilters.add(headerFilter);
        }
        return this;
    }
    
    public List<RoleFilter> getRoleFilters() {
        return roleFilters;
    }
    
    public RuleBuilder setRoleFilters(List<RoleFilter> roleFilters) {
        this.roleFilters = roleFilters;
        return this;
    }
    
    public RuleBuilder addRoleFilter(RoleFilter roleFilter) {
        if (roleFilters == null) {
            roleFilters = new ArrayList<>();
        }
        if (roleFilter != null) {
            roleFilters.add(roleFilter);
        }
        return this;
    }
    
    public List<UserFilter> getUserFilters() {
        return userFilters;
    }
    
    public RuleBuilder setUserFilters(List<UserFilter> userFilters) {
        this.userFilters = userFilters;
        return this;
    }
    
    public RuleBuilder addUserFilter(UserFilter userFilter) {
        if (userFilters == null) {
            userFilters = new ArrayList<>();
        }
        if (userFilter != null) {
            userFilters.add(userFilter);
        }
        return this;
    }
    
}
