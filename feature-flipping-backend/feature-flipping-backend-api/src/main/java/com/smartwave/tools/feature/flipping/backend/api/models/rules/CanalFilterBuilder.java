package com.smartwave.tools.feature.flipping.backend.api.models.rules;

import com.smartwave.tools.feature.flipping.backend.api.models.Builder;

public class CanalFilterBuilder implements Builder<CanalFilter> {
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    private Long   uid;
    
    private String canal;
    
    private Double percent;
    
    // =========================================================================
    // CONSTRUCTORS
    // =========================================================================
    public CanalFilterBuilder() {
    }
    
    public CanalFilterBuilder(Long uid, String canal, Double percent) {
        super();
        this.uid = uid;
        this.canal = canal;
        this.percent = percent;
    }
    
    @Override
    public CanalFilter build() {
        return new CanalFilter(uid, canal, percent);
    }
    
    @Override
    public CanalFilterBuilder clear() {
        uid = null;
        canal = null;
        percent = null;
        return this;
    }
    
    // =========================================================================
    // OVERRIDES
    // =========================================================================
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((uid == null) ? 0 : uid.hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        boolean result = this == obj;
        
        if (!result && obj != null && obj instanceof CanalFilterBuilder) {
            final CanalFilterBuilder other = (CanalFilterBuilder) obj;
            result = uid == null ? other.getUid() == null : uid.equals(other.getUid());
        }
        
        return result;
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("CanalFilter [uid=");
        builder.append(uid);
        builder.append(", canal=");
        builder.append(canal);
        builder.append(", percent=");
        builder.append(percent);
        builder.append("]");
        return builder.toString();
    }
    
    // =========================================================================
    // GETTERS & SETTERS
    // =========================================================================
    public Long getUid() {
        return uid;
    }
    
    public CanalFilterBuilder setUid(Long uid) {
        this.uid = uid;
        return this;
    }
    
    public String getCanal() {
        return canal;
    }
    
    public CanalFilterBuilder setCanal(String canal) {
        this.canal = canal;
        return this;
    }
    
    public Double getPercent() {
        return percent;
    }
    
    public CanalFilterBuilder setPercent(Double percent) {
        this.percent = percent;
        return this;
    }
    
}
