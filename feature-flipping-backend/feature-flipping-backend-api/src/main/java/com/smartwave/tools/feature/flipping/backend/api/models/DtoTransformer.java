package com.smartwave.tools.feature.flipping.backend.api.models;

@FunctionalInterface
public interface DtoTransformer<E, D> {
    D transform(final E entity);
}
