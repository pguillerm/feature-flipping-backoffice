package com.smartwave.tools.feature.flipping.backend.api.constants;

public final class Header {
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    public static final String DEVICE_IDENTIFIER            = "x-device-identifier";
    
    public static final String CORRELATION_ID               = "x-correlation-id";
    
    public static final String REQUEST_ID                   = "x-request-id";
    
    public static final String CONVERSATION_ID              = "x-conversation-id";
    
    public static final String CLIENT_VERSION               = "x-client-version";
    
    public static final String DEVICE_TYPE                  = "x-device-type";
    
    public static final String DEVICE_CLASS                 = "x-device-class";
    
    public static final String DEVICE_OS_VERSION            = "x-device-os-version";
    
    public static final String DEVICE_VERSION               = "x-device-version";
    
    public static final String DEVICE_NETWORK_TYPE          = "x-device-network-type";
    
    public static final String DEVICE_NETWORK_SPEED_DOWN    = "x-device-network-speed-down";
    
    public static final String DEVICE_NETWORK_SPEED_UP      = "x-device-network-speed-up";
    
    public static final String DEVICE_NETWORK_SPEED_LATENCY = "x-device-network-speed-latency";
    
    public static final String LANGUAGE                     = "x-language";
    
    public static final String COUNTRY                      = "x-country";
    
    public static final String DEVICE_IP                    = "x-device-ip";
    
    public static final String USER_AGENT                   = "User-Agent";
    
    // =========================================================================
    // CONSTRUCTORS
    // =========================================================================
    private Header() {
    }
    
}
