package com.smartwave.tools.feature.flipping.backend.api.models.rules;

import com.smartwave.tools.feature.flipping.backend.api.models.Builder;

public class HeaderFilterBuilder implements Builder<HeaderFilter> {
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    private Long   uid;
    
    private String name;
    
    private String value;
    
    // =========================================================================
    // CONSTRUCTORS
    // =========================================================================
    public HeaderFilterBuilder() {
    }
    
    public HeaderFilterBuilder(final Long uid, final String name, final String value) {
        super();
        this.uid = uid;
        this.name = name;
        this.value = value;
    }
    
    @Override
    public HeaderFilter build() {
        return new HeaderFilter(uid, name, value);
    }
    
    @Override
    public HeaderFilterBuilder clear() {
        uid = null;
        name = null;
        value = null;
        return this;
    }
    
    // =========================================================================
    // OVERRIDES
    // =========================================================================
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((uid == null) ? 0 : uid.hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        boolean result = this == obj;
        
        if (!result && obj != null && obj instanceof Rule) {
            final Rule other = (Rule) obj;
            result = uid == null ? other.getUid() == null : uid.equals(other.getUid());
        }
        
        return result;
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("HeaderFilter [uid=");
        builder.append(uid);
        builder.append(", name=");
        builder.append(name);
        builder.append(", value=");
        builder.append(value);
        builder.append("]");
        return builder.toString();
    }
    
    // =========================================================================
    // GETTERS & SETTERS
    // =========================================================================
    public Long getUid() {
        return uid;
    }
    
    public HeaderFilterBuilder setUid(Long uid) {
        this.uid = uid;
        return this;
    }
    
    public String getName() {
        return name;
    }
    
    public HeaderFilterBuilder setName(String name) {
        this.name = name;
        return this;
    }
    
    public String getValue() {
        return value;
    }
    
    public HeaderFilterBuilder setValue(String value) {
        this.value = value;
        return this;
    }
    
}
