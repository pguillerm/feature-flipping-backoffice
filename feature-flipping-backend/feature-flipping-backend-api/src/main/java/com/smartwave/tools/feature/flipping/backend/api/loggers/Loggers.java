package com.smartwave.tools.feature.flipping.backend.api.loggers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class Loggers {
    // =========================================================================
    // ATTRIBUTES
    // =========================================================================
    public static final Logger APPLICATION = LoggerFactory.getLogger("APPLICATION");
    
    public static final Logger CACHE       = LoggerFactory.getLogger("CACHE");
    
    public static final Logger CONFIG      = LoggerFactory.getLogger("CONFIG");
    
    public static final Logger CHRONOLOG   = LoggerFactory.getLogger("CHRONOLOG");
    
    public static final Logger XLLOG       = LoggerFactory.getLogger("XLLOG");
    
    public static final Logger INIT        = LoggerFactory.getLogger("INITIALIZE");
    
    public static final Logger DEBUG       = LoggerFactory.getLogger("DEBUGLOG");
    
    public static final Logger SYSTEM      = LoggerFactory.getLogger("SYSTEM");
    
    public static final Logger SECURITY    = LoggerFactory.getLogger("SECURITY");
    
    public static final Logger HEALTH      = LoggerFactory.getLogger("HEALTH");
    
    public static final Logger REST        = LoggerFactory.getLogger("REST");
}
