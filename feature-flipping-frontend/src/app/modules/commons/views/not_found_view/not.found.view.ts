import {Component} from '@angular/core';



@Component({
    template: `
<section class="features-home-view">
    <header>
        <h1>404 Not found</h1>
    </header>

    <content>
        Oups! Page not found :-(        
    </content>

</section>
 `,
 styleUrls     : ['./not.found.view.scss']
})
export class NotFoundView{
}