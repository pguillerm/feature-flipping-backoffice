import {BrowserModule}    from '@angular/platform-browser';
import {NgModule}         from '@angular/core';
import {HttpClientModule} from '@angular/common/http';

import {CONSTANTS}        from './constants/constants';
import {SessionScope}     from './scopes/session.scope';

import {HeaderService}    from './services/header.service';
import {HttpService}      from './services/http.service';
import {CacheService}     from './services/cache.service';

import {NotFoundView}     from './views/not_found_view/not.found.view';
@NgModule({
  declarations: [
    NotFoundView
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  exports:[
    NotFoundView,
  ],
  providers: [
    HeaderService,
    HttpService,
    SessionScope,
    CacheService
  ]
})
export class CommonsModule { }
