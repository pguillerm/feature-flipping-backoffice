export interface SelectItem<T> {
     value          : T,
     selected       : boolean,
     disabled       : boolean,
     label          : string,
     styleClass?    : string

}