import {Injectable}             from '@angular/core';
import {Observable}             from 'rxjs';
import {isNull,isNotNull}       from './../constants/checks';

@Injectable()
export class CacheService {
    /**************************************************************************
    * CONSTRUCTORS
    **************************************************************************/
    constructor() {
    }

    /**************************************************************************
    * API
    **************************************************************************/
    load(cacheKey : string):Observable<any>{
        return new Observable((observer) => {
            let result = null;
            if(isNotNull(cacheKey)){
                let rawData = localStorage.getItem(cacheKey);
                result =  isNull(rawData)?null : JSON.parse(rawData);
                observer.next(result);
                observer.complete();
            }
            return result;
        });
    }

    save(cacheKey : string, value : any){
        if(isNull(cacheKey)){
            throw "cache key is mandatory!";
        }
        
        if(isNotNull(value)){
            localStorage.setItem(cacheKey,JSON.stringify(value));
        }
    }

    remove(cacheKey : string){
        if(isNull(cacheKey)){
            throw "cache key is mandatory!";
        }
        localStorage.removeItem(cacheKey);
    }
}