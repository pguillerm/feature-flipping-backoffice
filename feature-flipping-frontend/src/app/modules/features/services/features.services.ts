import {Injectable}         from '@angular/core';
import {CONSTANTS}          from './../../commons/constants/constants';
import {environment}        from './../../../../environments/environment';
import {HttpService}        from './../../commons/services/http.service';

import {Feature}            from './../models/feature.interface';

@Injectable()
export class FeaturesServices {
    /***************************************************************************
    * ATTRIBUTES
    ***************************************************************************/
    
    /***************************************************************************
    * CONSTRUCTOR
    ***************************************************************************/
    constructor(private httpService : HttpService){
       
    }

    
    /***************************************************************************
    * API
    ***************************************************************************/
    public findAll() :  Promise<Feature[]>{
        return this.httpService.get(`${environment.urls.api}/feature`);
    }

    public deleteRule(featureUid:string, ruleUis:number): Promise<any>{
       return this.httpService.delete(`${environment.urls.api}/rule/${featureUid}/${ruleUis}`);
    }
}