export interface DeleteRuleEvent{
    featureUid  : string,
    ruleUid     : number
}