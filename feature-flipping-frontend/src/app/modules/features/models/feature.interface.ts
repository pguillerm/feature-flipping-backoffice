
export interface CanalFilter{
    uid                 : number,
    canal               : string,
    percent             : number
}

export interface SchedulerFilter{
    uid                 : number,
    duration?           : number,
    durationUnit?       : string,
    cronExpression?     : string
}

export interface HeaderFilter{
    uid                 : number,
    name                : string,
    value               : string
}

export interface UserFilter{
    uid                 : number,
    role                : string
}

export interface RoleFilter{
    uid                 : number,
    userName            : string
}

export interface Rule{
    uid                 : number,
    name                : string,
    canalFilters?       : CanalFilter[],
    schedulerFilter?    : SchedulerFilter,
    headerFilters?      : HeaderFilter[],
    roleFilters?        : RoleFilter[],
    userFilters?        : UserFilter[],
}

export interface Feature{
    uid             : string,
    title           : string,
    description     : string,
    screenshots?    : string[],
    assets?         : string[],
    rules?          : Rule[],
}