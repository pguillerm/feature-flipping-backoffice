import {BrowserModule}      from '@angular/platform-browser';
import {NgModule}           from '@angular/core';
import {FormsModule}        from '@angular/forms'
import {CommonsModule}      from './../commons/commons.module';
import {FontAwesomeModule}  from '@fortawesome/angular-fontawesome';

import {MatCheckboxModule}  from '@angular/material/checkbox';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatInputModule}     from '@angular/material/input';

//==== services ================================================================
import {FeaturesServices}   from './services/features.services';

//==== views ===================================================================
import {FeatureHomeView}    from './views/features.home.view';

//==== components ==============================================================
import {FeatureDisplay}     from './components/feature_display/feature.display';
import {RulePreviewDisplay} from './components/rule_preview_display/rule.preview.display';

@NgModule({
  declarations: [
    FeatureHomeView,
    FeatureDisplay,
    RulePreviewDisplay
  ],
  imports: [
    BrowserModule,
    CommonsModule,
    FontAwesomeModule,
    FormsModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatInputModule
  ],
  exports:[
    FeatureHomeView
  ],
  providers: [FeaturesServices]
})
export class FeaturesModule { }
