import {Component,Input, Output,EventEmitter}    from '@angular/core';
import {Rule}                                    from '../../models/feature.interface';

import {faTimes }                                from '@fortawesome/free-solid-svg-icons';
import {DeleteRuleEvent}                         from './../../models/rule.events.interface';

@Component({
    selector      : 'rule-preview-display',
    templateUrl   : './rule.preview.display.html',
    styleUrls     : ['./rule.preview.display.scss']
})
export class RulePreviewDisplay{
    /***************************************************************************
     * ATTRIBUTES
     **************************************************************************/
    @Input() styleClass : string;
    @Input() featureUid : string;
    @Input() value      : Rule;

    @Output() onDelete : EventEmitter<DeleteRuleEvent> = new EventEmitter();

    timesIcon           : any = faTimes;

    /***************************************************************************
    * EVENTS
    ***************************************************************************/
    delete(){
        this.onDelete.emit({featureUid:this.featureUid, ruleUid:this.value.uid});
    }
}