import {Component,Input,Output,EventEmitter}    from '@angular/core';
import {Feature}                                from '../../models/feature.interface';

import {DeleteRuleEvent}                         from './../../models/rule.events.interface';


@Component({
    selector      : 'feature-display',
    templateUrl   : './feature.display.html',
    styleUrls     : ['./feature.display.scss']
})
export class FeatureDisplay{
    /***************************************************************************
     * ATTRIBUTES
     **************************************************************************/
    @Input() styleClass : string;
    @Input() value      : Feature;

    @Output() onDeleteRule : EventEmitter<DeleteRuleEvent> = new EventEmitter();


    /***************************************************************************
     * EVENTS
     **************************************************************************/
    handlerRuleDelete(event:DeleteRuleEvent){
        this.onDeleteRule.emit(event);
    }

    notifyRuleDeleted(event:DeleteRuleEvent){
        if(this.value.uid === event.featureUid){
            let rules = this.value.rules.filter((rule)=>rule.uid!=event.ruleUid);
            this.value.rules = rules;
        }
    }
}