import {Component,OnInit,ViewChildren}   from '@angular/core';
import {Observable}                         from 'rxjs';
import {faAngleDoubleLeft,
        faAngleDoubleRight,
        faSync }                            from '@fortawesome/free-solid-svg-icons';

import {FeaturesServices}                   from './../services/features.services';
import {Feature}                            from './../models/feature.interface';
import {SelectItem}                         from './../../commons/models/select.item';
import {isNotNull,isNull,isNotEmpty}        from '../../commons/constants/checks';
import {DeleteRuleEvent}                    from './../models/rule.events.interface';
import {FeatureDisplay}                     from'./../components/feature_display/feature.display';

const _CACHE_KEY_FEATURES_DATA : string= "features-data";

@Component({
    templateUrl   : './features.home.view.html',
    styleUrls     : ['./features.home.view.scss']
})
export class FeatureHomeView implements OnInit{

    /**************************************************************************
    * ATTRIBUTES
    **************************************************************************/
    asideOpen           : boolean = false;
    features            : Feature[];
    displayFeatures     : Feature[];
    refreshIcon         : any = faSync;

    featureName         : string;
    assets              : SelectItem<string>[];

    @ViewChildren(FeatureDisplay) featureDisplayComponents : FeatureDisplay[];
    /**************************************************************************
    * CONSTRUCTORS
    **************************************************************************/
    constructor(private featuresServices : FeaturesServices){

    }

    ngOnInit() {
        this.loadFeaturesData();
    }

    /**************************************************************************
    * INITIALIZE
    **************************************************************************/
    loadFeaturesData(){
        this.featuresServices
            .findAll()
            .then((data)=> {
                    this.features = data;
                    this.initializeAssetsSelectItem();
                    this.initializeDisplayFeature();
        });
    }

    initializeAssetsSelectItem(){
        if(isNotNull(this.features)){
            let buffer = {};
            
            this.features.forEach((feature)=>{
                for(let asset of isNull(feature.assets)?[]:feature.assets){
                    if(isNull(buffer[asset])){
                        buffer[asset]= {
                            value          : asset,
                            selected       : true,
                            disabled       : false,
                            label          : asset
                        };
                    }
                }
            })
            
            let result = [];
            for(let key of Object.keys(buffer)){
                result.push(buffer[key]);
            }
            
            this.assets = result;
        }
    }

    initializeDisplayFeature(){
        if(isNull(this.features) || this.features.length==0){
            this.displayFeatures=[];
        }else{
            this.displayFeatures = this.features.filter((feature)=>{
                return  this.filterFeatureByName(feature) &&
                        this.filterFeatureByAssetsSelected(feature);
            });
        }
    }
    /**************************************************************************
    * filters
    **************************************************************************/
    private filterFeatureByName(feature:Feature):boolean{
        return isNull(this.featureName) || isNull(feature.title)?true : feature.title.toLowerCase().includes(this.featureName.toLowerCase());
    }

    private filterFeatureByAssetsSelected(feature:Feature):boolean{
        let result = true;
        console.log("filter change");
        if(isNotEmpty(this.assets) && isNotEmpty(feature.assets)){
            result = false;

            this.assets.forEach((selectItem)=>{
                if(!result && selectItem.selected){
                    for(let asset of feature.assets){
                        result = selectItem.value == asset;
                        if(result){
                            break;
                        }
                    }
                }
            })
        }
        return result;
    }

    handlerFilterChange(){
        this.initializeDisplayFeature();
    }
    /**************************************************************************
    * ASIDE MENU CONTROL
    **************************************************************************/
    toogleAsideMenu(){
        this.asideOpen = !this.asideOpen;
    }
    getAsideButtonIcon(){
        return this.asideOpen?faAngleDoubleLeft:faAngleDoubleRight;
    }


    handlerDeleteRule(event:DeleteRuleEvent){
        this.featuresServices
            .deleteRule(event.featureUid,event.ruleUid)
            .then(()=>this.notifyFeatureDisplayRuleDeleted(event));
    }

    private notifyFeatureDisplayRuleDeleted(event:DeleteRuleEvent ){
        console.log(this.featureDisplayComponents);
        this.featureDisplayComponents.forEach((compo)=>compo.notifyRuleDeleted(event));
    }
}