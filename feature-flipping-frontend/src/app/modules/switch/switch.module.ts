import {BrowserModule}    from '@angular/platform-browser';
import {NgModule}         from '@angular/core';
import {CommonsModule}    from './../commons/commons.module';

//==== services ================================================================

//==== views ===================================================================
import {SwitchHomeView}  from './views/switch.home.view';


@NgModule({
  declarations: [
    SwitchHomeView
  ],
  imports: [
    BrowserModule,
    CommonsModule
  ],
  exports:[
    SwitchHomeView
  ],
  providers: []
})
export class SwitchModule { }
