import { Component }        from '@angular/core';
import { environment }      from './../environments/environment';

interface Nav {
  path:string,
  label:string,
  styleClass:string
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'feature-flipping-frontend';

  links : Nav[] = [
    {path:"/"       , label:"Home"    ,styleClass:"home"},
    {path:"/switch" , label:"Switch"  ,styleClass:"switch"},
  ]

  constructor(){
    console.log(environment.urls.api);
  }
}
