import {BrowserModule}            from '@angular/platform-browser';
import {NgModule}                 from '@angular/core';
import {BrowserAnimationsModule}  from '@angular/platform-browser/animations';
import {FontAwesomeModule}        from '@fortawesome/angular-fontawesome';

import {AppRoutingModule}         from './app-routing.module';
import {AppComponent}             from './app.component';
import {environment}              from './../environments/environment';

import {CommonsModule}            from './modules/commons/commons.module';
import {FeaturesModule}           from './modules/features/features.module';
import {SwitchModule}             from './modules/switch/switch.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    AppRoutingModule,
    CommonsModule,
    FeaturesModule,
    SwitchModule
    
  ],
  exports:[],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
