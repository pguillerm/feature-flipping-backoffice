import {NgModule}             from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {NotFoundView}         from './modules/commons/views/not_found_view/not.found.view';
import {FeatureHomeView}      from './modules/features/views/features.home.view';
import {SwitchHomeView}       from './modules/switch/views/switch.home.view';



const routes: Routes = [
  
  {path: 'switch'           , component: SwitchHomeView},

  { path: ''  , component: FeatureHomeView, pathMatch: 'full'},
  { path: '**', component:NotFoundView},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
